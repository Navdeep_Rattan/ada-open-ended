#ifndef PARTITION_H_INCLUDED
#define PARTITION_H_INCLUDED

int partition(char arr[],int low,int high,char pivot)                      //find location of pivot for quick sort
{
   int i = low,temp1,temp2;
   for(int j=low; j<high; j++)
   {
      if(arr[j] <pivot)                                                    //when jth element less than pivot, swap ith and jth element
      {
         temp1=arr[i];
         arr[i]=arr[j];
         arr[j]=temp1;
         i++;
      }
      else if(arr[j] == pivot)                                             //when jth element is same as pivot, swap jth and last element
      {
         temp2=arr[j];
         arr[j]=arr[high];
         arr[high]=temp2;
         j--;
      }
   }
   temp1=arr[i];
   arr[i]=arr[high];
   arr[high]=temp1;
   return i;                                                              //the location of pivot element
}

#endif // PARTITION_H_INCLUDED
