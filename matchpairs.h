#ifndef MATCHPAIRS_H_INCLUDED
#define MATCHPAIRS_H_INCLUDED

void matchpair(char nuts[],char bolts[],int low,int high )
{
    if(low<high)
    {
        int pivot=partition(nuts,low,high,bolts[high]);                  //choose item from bolt to nut partitioning
        partition(bolts,low,high,nuts[pivot]);                           //place previous pivot location in bolt also

        matchpair(nuts,bolts,low,pivot-1);
        matchpair(nuts,bolts,pivot+1,high);

    }
}

#endif // MATCHPAIRS_H_INCLUDED
