#include <iostream>
#include "partition.h"
#include "matchpairs.h"
using namespace std;

void Display(char arr[], int n) {
   for(int i = 0; i<n; i++)
      cout<<arr[i]<<"  ";
}
int main()
{
   char nuts[] = {')','@','*','^','(','%','!','$','&','#'};
   char bolts[] = {'!','(','#','%',')','^','&','*','$','@'};
   int n = 10;
   matchpair(nuts, bolts, 0, n-1);
   cout<<"After matching nuts and bolts:"<<endl;
   cout<<"Nuts:  ";
   Display(nuts, n);
   cout<<endl;
   cout<<"Bolts: ";
   Display(bolts, n);
   cout<<endl;
   return 0;
}
